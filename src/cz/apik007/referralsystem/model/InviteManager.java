package cz.apik007.referralsystem.model;

import cz.apik007.referralsystem.utils.Options;
import cz.apik007.referralsystem.ReferralSystem;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InviteManager {

    private List<Invite> invites;
    private ReferralSystem plugin;

    public InviteManager() throws SQLException, ClassNotFoundException {
        invites = new ArrayList<>();
        plugin = ReferralSystem.getInstance();

        if (!plugin.isMysqlSetup()) {
            if (plugin.getInvites().getKeys(false) != null) {
                for (String invited : plugin.getInvites().getConfigurationSection("").getKeys(false)) {
                    Invite toAdd = new Invite(invited, plugin.getInvites().getString(invited + "." + Options.INV_INVITER), plugin.getInvites().getLong(invited + "." + Options.INV_TIME), plugin.getInvites().getBoolean(invited + "." + Options.INV_ACCEPTED), plugin.getInvites().getBoolean(invited + "." + Options.INV_REWARDED));
                    invites.add(toAdd);
                }
            }
        } else {
            plugin.getMysqlManager().getDb().openConnection();
            Statement statement = plugin.getMysqlManager().getDb().getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM `rsp_invites`;");
            while (result.next()) {
                Invite invite = new Invite(result.getString("invited"), result.getString("inviter"), result.getLong("time"), result.getBoolean("accepted"), result.getBoolean("rewarded"));
                invites.add(invite);
            }
            plugin.getMysqlManager().getDb().closeConnection();
        }
    }

    public Invite getInviteByInvited(String invited) {
        Invite toReturn = null;
        for (Invite invite : invites) {
            if (invite.getInvited().equalsIgnoreCase(invited)) {
                toReturn = invite;
                break;
            }
        }
        return toReturn;
    }

    public ArrayList<Invite> getInvitesByInviter(String inviter) {
        ArrayList<Invite> toReturn = new ArrayList<>();
        for (Invite invite : invites) {

            if (invite.getInviter().equals(inviter)) {
                toReturn.add(invite);
            }
        }
        return toReturn;
    }

    public void addInvite(String inviter, String invited) throws SQLException, ClassNotFoundException {
        if (getInviteByInvited(invited) == null) {
            long time = System.currentTimeMillis();
            Invite invite = new Invite(invited, inviter, time, false, false);
            invites.add(invite);
            if (!plugin.isMysqlSetup()) {
                plugin.getInvites().set(invited, "");
                plugin.getInvites().set(invited + "." + Options.INV_INVITER, inviter);
                plugin.getInvites().set(invited + "." + Options.INV_TIME, time);
                plugin.getInvites().set(invited + "." + Options.INV_ACCEPTED, false);
                plugin.getInvites().set(invited + "." + Options.INV_REWARDED, false);
                try {
                    plugin.getInvites().save(plugin.getInvitesFile());
                } catch (IOException ex) {
                    Logger.getLogger(InviteManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                plugin.getMysqlManager().getDb().openConnection();
                PreparedStatement statement = plugin.getMysqlManager().getDb().getConnection().prepareStatement("INSERT INTO `rsp_invites` (invited, inviter, time, accepted, rewarded) VALUES ( ?, ?, ?, ?, ?)");
                statement.setString(1, invited);
                statement.setString(2, inviter);
                statement.setLong(3, time);
                statement.setBoolean(4, false);
                statement.setBoolean(5, false);
                statement.execute();
                plugin.getMysqlManager().getDb().closeConnection();
            }
        }
    }

    public void acceptInvite(String invited) throws SQLException, ClassNotFoundException {
        if (getInviteByInvited(invited) != null) {
            getInviteByInvited(invited).setAccepted(true);

            plugin.getMysqlManager().getDb().openConnection();

            PreparedStatement statement = plugin.getMysqlManager().getDb().getConnection().prepareStatement("UPDATE `rsp_invites` SET accepted = ? WHERE invited = ?;");
            statement.setBoolean(1, true);
            statement.setString(2, invited);
            statement.execute();

            plugin.getMysqlManager().getDb().closeConnection();

        }
    }

    public void rewardInvite(String invited) throws SQLException, ClassNotFoundException {
        if (getInviteByInvited(invited) != null) {
            getInviteByInvited(invited).setRewarded(true);

            plugin.getMysqlManager().getDb().openConnection();

            PreparedStatement statement = plugin.getMysqlManager().getDb().getConnection().prepareStatement("UPDATE `rsp_invites` SET rewarded = ? WHERE invited = ?;");
            statement.setBoolean(1, true);
            statement.setString(2, invited);
            statement.execute();

            plugin.getMysqlManager().getDb().closeConnection();

        }
    }

    public void removeInviteByInvited(String name) throws SQLException, ClassNotFoundException {
        try {
            Invite invite = getInviteByInvited(name);

            Iterator<Invite> it = invites.iterator();
            while (it.hasNext()) {
                Invite value = it.next();
                if (value.getInvited().equalsIgnoreCase(invite.getInvited()) && value.getInviter().equalsIgnoreCase(invite.getInviter())) {
                    if (plugin.isMysqlSetup()) {
                        plugin.getMysqlManager().getDb().openConnection();
                        PreparedStatement statement = plugin.getMysqlManager().getDb().getConnection().prepareStatement("DELETE FROM `rsp_invites` WHERE invited = ? AND inviter = ?;");
                        statement.setString(1, invite.getInvited());
                        statement.setString(2, invite.getInviter());
                        statement.execute();
                        plugin.getMysqlManager().getDb().closeConnection();

                    } else {
                        plugin.getInvites().set(name, null);
                        try {
                            plugin.getInvites().save(plugin.getInvitesFile());
                        } catch (IOException ex) {
                            Logger.getLogger(InviteManager.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    invites.remove(value);
                }

            }
        } catch (ConcurrentModificationException e) {
        }

    }

    public List<Invite> getInvites() {
        return invites;
    }

}
