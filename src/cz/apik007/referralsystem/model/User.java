package cz.apik007.referralsystem.model;

public class User {

    private String name;
    private String UUID;
    private String IP;

    private long firstLog;
    private long lastLog;
    private long timePlayed;

    public User(String name, String UUID, String IP, long firstLog, long lastLog, long timePlayed) {
        this.name = name;
        this.UUID = UUID;
        this.IP = IP;

        this.firstLog = firstLog;
        this.lastLog = lastLog;
        this.timePlayed = timePlayed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public long getFirstLog() {
        return firstLog;
    }

    public void setFirstLog(long firstLog) {
        this.firstLog = firstLog;
    }

    public long getLastLog() {
        return lastLog;
    }

    public void setLastLog(long lastLog) {
        this.lastLog = lastLog;
    }

    public long getTimePlayed() {
        return timePlayed;
    }

    public void setTimePlayed(long timePlayed) {
        this.timePlayed = timePlayed;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

}
