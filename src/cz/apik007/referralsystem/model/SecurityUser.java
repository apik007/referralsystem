package cz.apik007.referralsystem.model;

public class SecurityUser {

    private String uuid;
    private long lastMoved;
    private long afkTime;
    private int blockMoved;

    public SecurityUser(String uuid, long afkTime, int blockMoved, long lastMoved) {
        this.uuid = uuid;
        this.afkTime = afkTime;
        this.blockMoved = blockMoved;
        this.lastMoved = lastMoved;
    }

    public String getName() {
        return uuid;
    }

    public void setName(String name) {
        this.uuid = name;
    }

    public long getAfkTime() {
        return afkTime;
    }

    public void setAfkTime(long afkTime) {
        this.afkTime = afkTime;
    }

    public int getBlockMoved() {
        return blockMoved;
    }

    public void setBlockMoved(int blockMoved) {
        this.blockMoved = blockMoved;
    }

    public long getLastMoved() {
        return lastMoved;
    }

    public void setLastMoved(long lastMoved) {
        this.lastMoved = lastMoved;
    }

}
