package cz.apik007.referralsystem.model;

import cz.apik007.referralsystem.ReferralSystem;
import cz.apik007.referralsystem.utils.Options;
import cz.apik007.referralsystem.utils.Utils;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bukkit.entity.Player;

public class SecurityUserManager {

    private List<SecurityUser> users;
    private ReferralSystem plugin;

    public SecurityUserManager() {
        users = new ArrayList<>();
        plugin = ReferralSystem.getInstance();
    }

    public void addUser(String name) {
        SecurityUser user = new SecurityUser(name, 0, 0, System.currentTimeMillis());
        users.add(user);
    }

    public void removeUser(String name) {
        Iterator<SecurityUser> it = users.iterator();
        while (it.hasNext()) {
            if (it.next().getName().equalsIgnoreCase(name)) {
                it.remove();
                break;
            }
        }
    }

    public SecurityUser getUser(String name) {
        SecurityUser user = null;
        for (SecurityUser user1 : users) {
            if (user1.getName().equalsIgnoreCase(name)) {
                user = user1;
            }
        }
        return user;
    }

    public void timeCounter(Player player) {
        try {
            if (player == null) {
                return;
            }

            User user = plugin.getUserManager().getUserByUUID(player.getUniqueId());

            if (user == null) {
                return;
            }

            long actualTime = System.currentTimeMillis();
            long lastCheck = user.getLastLog();
            plugin.getUserManager().setLastLog(player.getUniqueId());

            SecurityUser secUser = plugin.getSecurityUserManager().getUser(player.getName());
            if (secUser == null) {
                return;
            }

            long newTime = actualTime - lastCheck;

            long afkTime = secUser.getAfkTime();

            long oldTime = user.getTimePlayed();
            long total = newTime + oldTime - afkTime;

            secUser.setAfkTime(0);
            user.setTimePlayed(total);

            plugin.getMysqlManager().getDb().openConnection();

            PreparedStatement statement = plugin.getMysqlManager().getDb().getConnection().prepareStatement("UPDATE `rsp_users` SET timePlayed = ? WHERE uuid = ?;");
            statement.setLong(1, total);
            statement.setString(2, user.getUUID());
            statement.execute();

            plugin.getMysqlManager().getDb().closeConnection();
            
            plugin.getController().join(player);
        } catch (Exception ex) {

            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement se = ex.getStackTrace()[i];
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }

    }

    public void checkUser(Player player) {
        try {
            if (player == null) {
                return;
            }

            long time = System.currentTimeMillis();
            //Vytáhne si SecurityUsera
            SecurityUser user = plugin.getSecurityUserManager().getUser(player.getName());

            if (user == null) {
                return;
            }
            //Vytáhne si časový údaj LastMoved, který je logicky menší, než aktuální čas
            long lastMoved = user.getLastMoved();
            //Vytvoří proměnnou addAfkTime, která je rovna rozdílu time a lastMoved (tedy čas, kdy se frajer flákal)
            long addAfkTime = time - lastMoved;
            //Vezme si z configu proměnnou afkTimeLimit, která je v skeundách
            long afkTimeLimit = plugin.getConfig().getLong(Options.AFK_TIME) * 1000;
            //Pokud je afkTime není vyšší, nebo roven limitu, nastav na nula
            if (addAfkTime < afkTimeLimit) {
                addAfkTime = 0;
            }
            user.setAfkTime(user.getAfkTime() + addAfkTime);
            user.setLastMoved(time);
        } catch (Exception ex) {

            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement se = ex.getStackTrace()[i];
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }
    }

}
