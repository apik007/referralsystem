package cz.apik007.referralsystem.model;

import cz.apik007.referralsystem.ReferralSystem;
import cz.apik007.referralsystem.utils.Utils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import org.bukkit.entity.Player;

public class UserManager {

    private List<User> users;
    private ReferralSystem plugin;

    public UserManager() {
        try {
            users = new CopyOnWriteArrayList<>();
            plugin = ReferralSystem.getInstance();

            plugin.getMysqlManager().getDb().openConnection();

            Statement statement = plugin.getMysqlManager().getDb().getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM `rsp_users`;");

            while (result.next()) {
                User user = new User(result.getString("name"), result.getString("uuid"), result.getString("ip"), result.getLong("firstLog"), result.getLong("lastLog"), result.getLong("timePlayed"));
                users.add(user);
            }
            plugin.getMysqlManager().getDb().closeConnection();
        } catch (Exception ex) {

            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement se = ex.getStackTrace()[i];
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }
    }

    public void newUser(Player player) {
        try {
            String playerName = player.getName();
            UUID playerUUID = player.getUniqueId();
            long time = System.currentTimeMillis();
            plugin.getMysqlManager().getDb().openConnection();

            if (this.getUserByUUID(playerUUID) == null) {
                PreparedStatement statement = plugin.getMysqlManager().getDb().getConnection().prepareStatement("INSERT INTO `rsp_users` (name, uuid, ip, firstLog, lastLog, timePlayed) VALUES ( ?, ?, ?, ?, ?, ?)");
                statement.setString(1, playerName);
                statement.setString(2, playerUUID.toString());
                statement.setString(3, player.getAddress().getHostString());
                statement.setLong(4, time);
                statement.setLong(5, time);
                statement.setLong(6, 0);
                statement.execute();
                User toAdd = new User(playerName, player.getUniqueId().toString(), player.getAddress().getHostString(), time, time, 0);
                users.add(toAdd);
            } else {
                PreparedStatement statement = plugin.getMysqlManager().getDb().getConnection().prepareStatement("UPDATE `rsp_users` SET ip = ?, lastLog = ? WHERE uuid = ?;");
                statement.setString(1, player.getAddress().getHostString());
                statement.setLong(2, time);
                statement.setString(3, playerUUID.toString());
                statement.execute();

                this.getUserByUUID(playerUUID).setIP(player.getAddress().getHostString());
                this.getUserByUUID(playerUUID).setLastLog(time);
            }
            plugin.getMysqlManager().getDb().closeConnection();
        } catch (Exception ex) {

            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement se = ex.getStackTrace()[i];
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }
    }

    public void setLastLog(UUID uuid) {
        try {
            if (this.getUserByUUID(uuid) == null) {
                Utils.sendConsoleMessage("&e[ReferralSystem] INTERNAL ERROR: getUserByUUID(" + uuid.toString() + ") == null IN UserManager.setLastLog(" + uuid.toString() + ")");
                return;
            }

            long time = System.currentTimeMillis();
            plugin.getMysqlManager().getDb().openConnection();
            PreparedStatement statement = plugin.getMysqlManager().getDb().getConnection().prepareStatement("UPDATE `rsp_users` SET lastLog = ? WHERE uuid = ?;");
            statement.setLong(1, time);
            statement.setString(2, uuid.toString());
            statement.execute();
            plugin.getMysqlManager().getDb().closeConnection();
            this.getUserByUUID(uuid).setLastLog(time);
        } catch (Exception ex) {

            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement se = ex.getStackTrace()[i];
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }
    }

    public List<User> getUsers() {
        return users;
    }

    public User getUserByName(String name) {
        User toReturn = null;
        for (User user : users) {
            if (user.getName().equalsIgnoreCase(name)) {
                toReturn = user;
            }
        }

        return toReturn;
    }

    public User getUserByUUID(UUID uuid) {
        User toReturn = null;
        for (User user : users) {
            if (user.getUUID().equalsIgnoreCase(uuid.toString())) {
                toReturn = user;
            }
        }

        return toReturn;
    }

}
