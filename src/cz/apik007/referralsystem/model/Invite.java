package cz.apik007.referralsystem.model;

public class Invite {

    /**
     * Object for invitation
     * <p>
     * @param inviter an UUID of inviting player
     * @param invited a name of player which is invited
     * @param time a time of invitation in ms
     * @param accepted a value which determines if the invited player joined
     * server and the conditions were OK
     * @param rewarded a value which determines if the rewards were sent
     */
    private String inviter; //UUID
    private String invited; //NAME
    private long time;
    private boolean accepted;
    private boolean rewarded;

    public Invite(String invited, String inviter, long time, boolean accepted, boolean rewarded) {
        this.inviter = inviter;
        this.invited = invited;
        this.time = time;
        this.accepted = accepted;
        this.rewarded = rewarded;
    }

    public String getInviter() {
        return inviter;
    }

    public void setInviter(String inviter) {
        this.inviter = inviter;
    }

    public String getInvited() {
        return invited;
    }

    public void setInvited(String invited) {
        this.invited = invited;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isRewarded() {
        return rewarded;
    }

    public void setRewarded(boolean rewarded) {
        this.rewarded = rewarded;
    }

}
