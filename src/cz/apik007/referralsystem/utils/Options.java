package cz.apik007.referralsystem.utils;

public class Options {

    public static final String CMD_NAME = "referral";
    public static final String CMD_ALIAS = "ref";

    public static final String CONFIG_USE_MYSQL = "useMysql";
    public static final String CONFIG_LIMIT = "limit";
    public static final String CONFIG_VIP_LIMIT = "vipLimit";
    public static final String CONFIG_TIMELIMIT = "playedTimeLimit";
    public static final String CONFIG_TOPLIMIT = "topPlayerLimit";
    public static final String CONFIG_MAX_ACCEPTABLE_PLAYED_TIME = "maxPlayedTime";
    public static final String AFK_TIME = "afkTime";
    public static final String CONFIG_SECTION_COMMANDS = "commands";
    public static final String CONFIG_SECTION_COMMANDS_NORMAL = "basic";
    public static final String CONFIG_SECTION_COMMANDS_REFERRER = "referrer";
    public static final String CONFIG_SECTION_COMMANDS_INVITED = "invited"; 

    public static final String ARG_LIST = "list";
    public static final String ARG_INFO = "info";
    public static final String ARG_LIMIT = "limit";
    public static final String ARG_INVITE = "invite";
    public static final String ARG_HELP = "help";
    public static final String ARG_MENU = "menu";
    public static final String ARG_TOP = "top";
    public static final String ARG_BYPASS = "bypass";

    public static final String PLR_IP = "ip";
    public static final String PLR_TIMESTAMPS_FIRSTLOG = "firstLogin";
    public static final String PLR_TIMESTAMPS_LASTLOG = "lastLogin";
    public static final String PLR_TIMESTAMPS_TIMEPLAYED = "timePlayed";
    public static final String PLR_REFERRAL_INVITES = "invites";
    public static final String PLR_REFERRAL_INVITER = "inviter";

    public static final String INV_INVITER = "inviter";
    public static final String INV_TIME = "time";
    public static final String INV_ACCEPTED = "accepted";
    public static final String INV_REWARDED = "rewarded";

    //public static final String PERMISSION_VIP = "rsp.vip";
    public static final String PERMISSION_ADMIN = "rsp.admin";
    public static final String PERMISSION_LIMIT = "rsp.limit";

}
