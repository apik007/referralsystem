package cz.apik007.referralsystem.utils;

public class MessagesStrings {

    public static final String PREXIF = "PREFIX";
    public static final String NO_INVITE = "NO_INVITE";
    public static final String ALREADY_INVITED = "ALREADY_INVITED";
    public static final String LIMIT_REACHED = "LIMIT_REACHED";
    public static final String ACTUAL_LIMIT = "ACTUAL_LIMIT";
    public static final String INGAME_ONLY = "INGAME_ONLY";
    public static final String CANNOT_INVITE_YOURSELF = "CANNOT_INVITE_YOURSELF";
    public static final String INVITED_SUCCESSFULY = "INVITED_SUCCESSFULY";
    public static final String SAME_IP = "SAME_IP";
    public static final String REF_INFO = "REF_INFO";
    public static final String ALREADY_PLAYING = "ALREADY_PLAYING";
    public static final String DOESNT_PLAYER_INVITATION = "DOESNT_PLAYER_INVITATION";
    public static final String ALREADY_ACCEPTED = "ALREADY_ACCEPTED";
    public static final String SUCCESSFULLY_REMOVED = "SUCCESSFULLY_REMOVED";
    public static final String INVITATION_DONE = "INVITATION_DONE";
    public static final String INVITATION_DONE_MULTI = "INVITATION_DONE_MULTI";
    public static final String INVITATIONS_ALREADY_SENT = "INVITATION_ALREADY_SENT";
    public static final String TOP = "TOP";
    
    public static final String SUCCESSFULLY_BYPASSED = "SUCCESSFULLY_BYPASSED";

    public static final String INVITED_PLAYER = "INVITED_PLAYER";
    public static final String REFERRER = "REFERRER";
    public static final String PROCESSED = "PROCESSED";
    public static final String TIME = "TIME";

    public static final String MENU_HOME = "MENU_HOME";
    public static final String MENU_DELETE = "MENU_DELETE";
    public static final String MENU_BACK = "MENU_BACK";
    public static final String MENU_INV_CONFIRM = "MENU_INV_CONFIRM";
    public static final String MENU_ADMIN_INVITEDBY = "MENU_ADMIN_INVITEDBY";
    public static final String MENU_INV_STATE = "MENU_INV_DETAIL";
    public static final String MENU_INV_DELETE = "MENU_INV_DELETE";
    public static final String MENU_INV_LETBE = "MENU_INV_LETBE";
    public static final String MENU_INV_ALREADY_DONE = "MENU_INV_ALREADY_DONE";
    public static final String MENU_INVITATIONS_NONE = "MENU_INVITATIONS_NONE";
    public static final String MENU_INVITATIONS_NONE_LORE = "MENU_INVITATIONS_NONE_LORE";

    public static final String HELP_INVITE = "HELP_INVITE";
    public static final String HELP_LIMIT = "HELP_LIMIT";
    public static final String HELP_INFO = "HELP_INFO";
    public static final String HELP_LIST = "HELP_LIST";
    public static final String HELP_REMOVE = "HELP_REMOVE";
    public static final String HELP_MENU = "HELP_MENU";
    public static final String HELP_HELP = "HELP_HELP";
    public static final String HELP_TOP = "HELP_TOP";
    public static final String HELP_BYPASS = "HELP_BYPASS";
    
    
    
    
    public static final String NO_PERMISSION = "NO_PERMISSION";

}
