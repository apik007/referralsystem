package cz.apik007.referralsystem.utils;

import cz.apik007.referralsystem.ReferralSystem;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang.StringUtils;
import static org.bukkit.Bukkit.getServer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.PermissionAttachmentInfo;

public class Utils {

    public static String colorizer(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static String serializeLocation(Location loc) {
        return loc.getWorld().getName() + "=" + loc.getX() + "=" + loc.getY() + "=" + loc.getZ() + "=" + loc.getPitch() + "=" + loc.getYaw();
    }

    public static boolean isDouble(String str) {
        String regExp = "[\\x00-\\x20]*[+-]?(((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*";
        boolean matches = str.matches(regExp);
        return matches;
    }

    public static boolean isInteger(String str) {
        boolean toReturn = false;
        try {
            int op1 = Integer.parseInt(str);
            toReturn = true;
        } catch (NumberFormatException e) {
            toReturn = false;
        }
        return toReturn;
    }

    public static void sendRegularMessage(Player player, String string) {
        ReferralSystem plugin = ReferralSystem.getInstance();
        player.sendMessage(Utils.colorizer(plugin.getMessages().getString(MessagesStrings.PREXIF) + plugin.getMessages().getString(string)));
    }

    public static Location unserializeLocation(String loc) {

        if (loc.equals("NOEXIST")) {
            return null;
        }

        String[] split = loc.split("=");
        return new Location(getServer().getWorld(split[0]),
                Double.parseDouble(split[1]), Double.parseDouble(split[2]),
                Double.parseDouble(split[3]), Float.parseFloat(split[4]),
                Float.parseFloat(split[5]));
    }

    public static ItemStack createItem(Material m, String name, List<String> lore) {
        ItemStack is = new ItemStack(m);
        ItemMeta i = is.getItemMeta();
        i.setDisplayName(name);
        if (lore != null) {
            i.setLore(lore);
        }
        is.setItemMeta(i);
        return is;
    }

    public static void setMessage(FileConfiguration messages, String string, String text) {
        if (!messages.contains(string)) {
            messages.set(string, text);
        }
    }

    public static boolean playerHasEnoughLimit(Player p, Integer count, String perm) {
        //p = hráč
        //count = počet, který chceme ověřit
        //perm string bez čísla

        for (int i = 1; i < 500; i++) {
            if (p.hasPermission(perm + "." + i) && count == i) {
                return true;
            }
        }
        return true;
    }

    public static Integer getLimit(Player p, String perm) {
        int limit = 0;
        for (int i = 1; i < 500; i++) {
            if (p.hasPermission(perm + "." + i)) {
                if (limit < i) {
                    limit = i;
                }
            }
        }
        return limit;
    }

    public static void sendConsoleMessage(String message) {
        getServer().getConsoleSender().sendMessage(colorizer(message));
    }
    
    public static void makeGlass(int id, Inventory inv) {
        ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) id);
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(Utils.colorizer("&0###"));
        item.setItemMeta(im);

        for (int i = 0; i < inv.getContents().length; i++) {
            ItemStack[] help = inv.getContents();
            if (help[i] == null) {
                Utils.setItem(i, item, inv);
            }
        }

    }

    public static void setItem(int position, ItemStack is, Inventory inv) {
        inv.setItem(position, is);
    }

    public static ItemStack createItem(Material material, String name, String lore) {
        ItemStack is = new ItemStack(material);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(Utils.colorizer(name));
        im.setLore(Arrays.asList(Utils.colorizer(lore)));
        is.setItemMeta(im);
        return is;
    }
    
    public static void setUpMessages(ReferralSystem plugin) {
        FileConfiguration messages = plugin.getMessages();

        setMessage(messages, MessagesStrings.PREXIF, "&a&l[&f&lReferralSystem&a&l]&b&l ");
        setMessage(messages, MessagesStrings.NO_INVITE, "&cSorry, but you haven't invited this player yet");
        setMessage(messages, MessagesStrings.ALREADY_INVITED, "&cSorry, but this player is already invited by another player");
        setMessage(messages, MessagesStrings.LIMIT_REACHED, "&cSorry, but you reached the limit of max. invitations");
        setMessage(messages, MessagesStrings.ACTUAL_LIMIT, "&aYour actual invitations limit is: &c");
        setMessage(messages, MessagesStrings.INGAME_ONLY, "[ReferralSystemPro] Sorry, but this command is supported only in-game");
        setMessage(messages, MessagesStrings.CANNOT_INVITE_YOURSELF, "&cSorry, but you can't invite yourself");
        setMessage(messages, MessagesStrings.INVITED_SUCCESSFULY, "&bYour invitation was sucessfuly saved!");
        setMessage(messages, MessagesStrings.SAME_IP, "&cYou were invited by another player, but the player has same IP as you");
        setMessage(messages, MessagesStrings.REF_INFO, "&bYou had been rewarded!");
        setMessage(messages, MessagesStrings.ALREADY_PLAYING, "&cSorry, but this player is already playing on this server!");
        setMessage(messages, MessagesStrings.DOESNT_PLAYER_INVITATION, "&cSorry, but you didn't invite this player!");
        setMessage(messages, MessagesStrings.ALREADY_ACCEPTED, "&cSorry, but this invitation is already done and processed!");
        setMessage(messages, MessagesStrings.SUCCESSFULLY_REMOVED, "&bYour invitation was successfully removed! 1 slot is free now");
        setMessage(messages, MessagesStrings.INVITATION_DONE, "&bThe player %referrer% was awarded, because he invited the %invited% !");
        setMessage(messages, MessagesStrings.INVITATION_DONE_MULTI, "&bThe player %referrer% was &aspecial awarded &b, because he has already %num% processed invitations!");
        setMessage(messages, MessagesStrings.MENU_BACK, "&c<<< BACK!");
        setMessage(messages, MessagesStrings.MENU_HOME, "&b&lReferral Menu");
        setMessage(messages, MessagesStrings.MENU_INV_STATE, "&aProcessed: ");
        setMessage(messages, MessagesStrings.MENU_DELETE, "&c&lRemoving...");
        setMessage(messages, MessagesStrings.MENU_INV_CONFIRM, "&cDo you really want to delete this invitation?");
        setMessage(messages, MessagesStrings.MENU_INV_ALREADY_DONE, "&cThis invitation is already processed !");
        setMessage(messages, MessagesStrings.MENU_INV_DELETE, "&cRemove the invitation");
        setMessage(messages, MessagesStrings.MENU_INV_LETBE, "&aCancel");
        setMessage(messages, MessagesStrings.MENU_ADMIN_INVITEDBY, " &b&linvited by &c&l");
        setMessage(messages, MessagesStrings.MENU_INVITATIONS_NONE, "&c&lNo invitations there");
        setMessage(messages, MessagesStrings.MENU_INVITATIONS_NONE_LORE, "&cNo invitations were found");
        setMessage(messages, MessagesStrings.TOP, "&b&lTop Referrers");
        setMessage(messages, MessagesStrings.INVITED_PLAYER, "&a&lInvited Player: &b&l");
        setMessage(messages, MessagesStrings.REFERRER, "&aInviter: ");
        setMessage(messages, MessagesStrings.PROCESSED, "&aProcessed: ");
        setMessage(messages, MessagesStrings.TIME, "&aTime: ");

        setMessage(messages, MessagesStrings.HELP_HELP, "&aShows this help menu");
        setMessage(messages, MessagesStrings.HELP_MENU, "&aShows GUI menu");
        setMessage(messages, MessagesStrings.HELP_INFO, " &aInfo about player's invitation");
        setMessage(messages, MessagesStrings.HELP_TOP, "&aShows top referrers");
        setMessage(messages, MessagesStrings.HELP_INVITE, "&aInvites a new player");
        setMessage(messages, MessagesStrings.HELP_LIMIT, "&aShows your max invitations limit");
        setMessage(messages, MessagesStrings.HELP_LIST, "&aShows a list of your invitations");
        setMessage(messages, MessagesStrings.HELP_REMOVE, "&aRemoves a invitationu");
        setMessage(messages, MessagesStrings.HELP_BYPASS, "&aSet the invitation as accepted - admin only");

        setMessage(messages, MessagesStrings.SUCCESSFULLY_BYPASSED, "&aYou successfully bypassed the invitation check proccess");

        setMessage(messages, MessagesStrings.NO_PERMISSION, "&cYou don't have enough permissions to do that");
        setMessage(messages, MessagesStrings.INVITATIONS_ALREADY_SENT, "&aAnd you have already sent &c%invites%&a invites");

        try {
            messages.save(plugin.getMessagesFile());
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order) {

        List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Entry<String, Integer>>() {
            public int compare(Entry<String, Integer> o1,
                    Entry<String, Integer> o2) {
                if (order) {
                    return o1.getValue().compareTo(o2.getValue());
                } else {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

}
