package cz.apik007.referralsystem.controller;

import cz.apik007.referralsystem.utils.MessagesStrings;
import cz.apik007.referralsystem.utils.Options;
import cz.apik007.referralsystem.utils.Utils;
import cz.apik007.referralsystem.*;
import cz.apik007.referralsystem.listeners.ActivityListener;
import cz.apik007.referralsystem.model.Invite;
import cz.apik007.referralsystem.model.InviteManager;
import cz.apik007.referralsystem.model.User;
import cz.apik007.referralsystem.model.UserManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Controller {

    private final ReferralSystem plugin = ReferralSystem.getInstance();
    private final InviteManager im = plugin.getInviteManager();
    private final UserManager um = plugin.getUserManager();

    public void invitePlayer(Player player, String playerName) {
        try {
            //Check if player is already invited
            if (im.getInviteByInvited(playerName) != null) {
                Utils.sendRegularMessage(player, MessagesStrings.ALREADY_INVITED);
                return;
            }
            //Check if player is not already playing 
            if (um.getUserByName(playerName) != null) {
                Utils.sendRegularMessage(player, MessagesStrings.ALREADY_PLAYING);
                return;
            }
            //Check if player will reach the limit
            if (!im.getInvitesByInviter(player.getName()).isEmpty() && (im.getInvitesByInviter(player.getName()).size() >= Utils.getLimit(player, Options.PERMISSION_LIMIT))) {
                Utils.sendRegularMessage(player, MessagesStrings.LIMIT_REACHED);
                return;
            }
            im.addInvite(player.getUniqueId().toString(), playerName);
            Utils.sendRegularMessage(player, MessagesStrings.INVITED_SUCCESSFULY);
        } catch (Exception ex) {

            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement se = ex.getStackTrace()[i];
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }
    }

    public void invitedPlayerJoined(Player player) throws SQLException, ClassNotFoundException {
        //Just checking if there isn't any error
        if (im.getInviteByInvited(player.getName()) == null) {
            return;
        }
        User invited = um.getUserByName(player.getName());
        long timeLimit = plugin.getConfig().getLong(Options.CONFIG_TIMELIMIT) * 1000;

        //Player doesn't reach the timeLimit yet
        if (invited.getTimePlayed() < timeLimit) {
            return;
        }
        User inviter = um.getUserByUUID(UUID.fromString(im.getInviteByInvited(player.getName()).getInviter()));
        //
        if (inviter.getIP().equals(invited.getIP())) {
            Utils.sendRegularMessage(player, MessagesStrings.SAME_IP);
            removeInvitation(player.getName());
            return;
        }

        im.acceptInvite(player.getName());
        rewardPlayer(player, UUID.fromString(im.getInviteByInvited(player.getName()).getInviter()), player.getName(), false);
    }

    public void rewardPlayer(Player player, UUID inviter, String invited, boolean referrer) {
        User inviterUser = um.getUserByUUID(inviter);

        String path = Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_REFERRER;
        if (!referrer) {
            path = Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_INVITED;
        }

        for (String command : (ArrayList<String>) plugin.getConfig().getList(path + "." + Options.CONFIG_SECTION_COMMANDS_NORMAL)) {
            command = command.replace("%referrer%", inviterUser.getName());
            command = command.replace("%invited%", invited);
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);

        }
        Utils.sendRegularMessage(player, MessagesStrings.REF_INFO);

        String str = plugin.getMessages().getString(MessagesStrings.INVITATION_DONE);
        str = str.replace("%referrer%", inviterUser.getName());
        str = str.replace("%invited%", invited);
        Bukkit.broadcastMessage(Utils.colorizer(str));
        int numberOfDoneInvsnumberOfDoneInvs = im.getInvitesByInviter(inviter.toString()).size();

        for (String number : plugin.getConfig().getConfigurationSection(path).getKeys(false)) {
            if (!isInteger(number)) {
                continue;
            }

            int num = Integer.parseInt(number);
            if (numberOfDoneInvsnumberOfDoneInvs != num) {
                continue;
            }
            for (String command : (ArrayList<String>) plugin.getConfig().getList(path + "." + num)) {
                str = str.replace("%referrer%", inviterUser.getName());
                str = str.replace("%invited%", invited);
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
                String str2 = plugin.getMessages().getString(MessagesStrings.INVITATION_DONE_MULTI);
                str2 = str2.replace("%referrer%", inviterUser.getName());
                str2 = str2.replace("%num%", String.valueOf(num));
                Bukkit.broadcastMessage(Utils.colorizer(str2));
            }

        }
    }

    public void checkReward(Player inviter) {

        for (Invite invite : plugin.getInviteManager().getInvitesByInviter(inviter.getUniqueId().toString())) {

            if (!invite.isAccepted() || invite.isRewarded()) {
                continue;
            }
            try {
                plugin.getController().rewardPlayer(inviter, UUID.fromString(invite.getInviter()), invite.getInvited(), true);
                plugin.getInviteManager().rewardInvite(invite.getInvited());
            } catch (Exception ex) {
                Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
                for (StackTraceElement se : ex.getStackTrace()) {
                    Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
                }

            }

        }
    }

    public void list(Player player) {
        List<Invite> invites = new ArrayList<>();
        if (player.hasPermission(Options.PERMISSION_ADMIN)) {
            invites = im.getInvites();
        } else {
            invites = im.getInvitesByInviter(player.getName());
        }
        StringBuilder builder = new StringBuilder("&b&m----------------&r &aLIST&r &b&m----------------\n");
        for (Invite invite : invites) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(invite.getTime());
            int mYear = calendar.get(Calendar.YEAR);
            int mMonth = calendar.get(Calendar.MONTH);
            int mDay = calendar.get(Calendar.DAY_OF_MONTH);
            builder.append("&b" + invite.getInvited() + " &c>>> " + plugin.getMessages().getString(MessagesStrings.PROCESSED) + "&b" + invite.isAccepted() + " " + plugin.getMessages().getString(MessagesStrings.TIME) + "&b" + mDay + "." + mMonth + "." + mYear + "\n");
        }
        player.sendMessage(Utils.colorizer(builder.toString()));
    }

    public void info(Player player, String name) {
        if (im.getInviteByInvited(name) == null) {
            Utils.sendRegularMessage(player, MessagesStrings.NO_INVITE);
            return;
        }

        Invite invite = im.getInviteByInvited(name);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(invite.getTime());
        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        StringBuilder builder = new StringBuilder("&b&m----------------&r " + plugin.getMessages().getString(MessagesStrings.INVITED_PLAYER) + name + "&r &b&m----------------\n");
        builder.append(plugin.getMessages().getString(MessagesStrings.REFERRER) + plugin.getUserManager().getUserByUUID(UUID.fromString(invite.getInviter())).getName() + "\n");
        builder.append(plugin.getMessages().getString(MessagesStrings.TIME) + mDay + "." + mMonth + "." + mYear + "\n");
        builder.append(plugin.getMessages().getString(MessagesStrings.PROCESSED) + invite.isAccepted() + "\n");

        player.sendMessage(Utils.colorizer(builder.toString()));

    }

    public void removeInvitation(Player player, String invited) {
        try {

            if (im.getInviteByInvited(invited) == null) {
                Utils.sendRegularMessage(player, MessagesStrings.NO_INVITE);
                return;
            }
            if (!im.getInviteByInvited(invited).getInviter().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission(Options.PERMISSION_ADMIN)) {
                Utils.sendRegularMessage(player, MessagesStrings.DOESNT_PLAYER_INVITATION);
                return;
            }
            if (im.getInviteByInvited(invited).isAccepted()) {
                Utils.sendRegularMessage(player, MessagesStrings.ALREADY_ACCEPTED);
                return;
            }
            im.removeInviteByInvited(invited);
            Utils.sendRegularMessage(player, MessagesStrings.SUCCESSFULLY_REMOVED);
        } catch (Exception ex) {
            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (StackTraceElement se : ex.getStackTrace()) {
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }
    }

    public void removeInvitation(String invited) {
        try {

            if (im.getInviteByInvited(invited) == null) {
                return;
            }
            if (im.getInviteByInvited(invited).isAccepted()) {
                return;
            }
            im.removeInviteByInvited(invited);
        } catch (Exception ex) {
            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (StackTraceElement se : ex.getStackTrace()) {
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }
    }

    public void helpMenu(Player player) {
        StringBuilder builder = new StringBuilder("&b&m----------------&r &a" + plugin.getMessages().getString(MessagesStrings.PREXIF) + "&r &b&m----------------\n");
        builder.append("&b/ref invite <playerName> " + plugin.getMessages().getString(MessagesStrings.HELP_INVITE) + "\n");
        builder.append("&b/ref info <playerName> " + plugin.getMessages().getString(MessagesStrings.HELP_INFO) + "\n");
        builder.append("&b/ref limit " + plugin.getMessages().getString(MessagesStrings.HELP_LIMIT) + "\n");
        builder.append("&b/ref list " + plugin.getMessages().getString(MessagesStrings.HELP_LIST) + "\n");
        builder.append("&b/ref remove <playerName> " + plugin.getMessages().getString(MessagesStrings.HELP_REMOVE) + "\n");
        builder.append("&b/ref help " + plugin.getMessages().getString(MessagesStrings.HELP_HELP) + "\n");
        builder.append("&b/ref menu " + plugin.getMessages().getString(MessagesStrings.HELP_MENU) + "\n");
        builder.append("&b/ref top " + plugin.getMessages().getString(MessagesStrings.HELP_TOP) + "\n");
        builder.append("&b/ref bypass <invitedPlayer> " + plugin.getMessages().getString(MessagesStrings.HELP_BYPASS) + "\n");
        player.sendMessage(Utils.colorizer(builder.toString()));
    }

    public void maybeReload() throws SQLException, ClassNotFoundException {
        boolean reload = false;
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            if (um.getUserByUUID(player.getUniqueId()) == null || plugin.getSecurityUserManager().getUser(player.getName()) == null) {
                reload = true;
            }
            join(player);
        }
        if (reload) {
            Utils.sendConsoleMessage("&c[ReferralSystem] WARNING: DO NOT USE RELOAD! IT MAY OCCURE BUGS AND IT CAN BREAK THE DATA!");
        }
    }

    public void join(Player player) throws SQLException, ClassNotFoundException {
        //Zkusíme vytvořit týpka

        plugin.getUserManager().newUser(player);

        //Zkoukneme, jestli náhodou není hráč invitnutý a jestli už není v procesu odměny
        if (plugin.getInviteManager().getInviteByInvited(player.getName()) != null && !plugin.getInviteManager().getInviteByInvited(player.getName()).isAccepted()) {
            plugin.getController().invitedPlayerJoined(player);
        }

        //Vytvoříme pomocného týpka, který nám bude hlídat čas
        plugin.getSecurityUserManager().addUser(player.getName());

        //Zkoukneme odměnu, jestli někoho nepozval a už se to mezitím v době nepřítomnosti nevyřídilo
        plugin.getController().checkReward(player);
    }

    public void topMenu(Player player) {
        StringBuilder builder = new StringBuilder("&b&m----------------&r &a" + plugin.getMessages().getString(MessagesStrings.TOP) + "&r &b&m----------------\n");

        HashMap<String, Integer> top = new HashMap<>();
        for (User user : um.getUsers()) {
            int number = 0;
            if (im.getInvitesByInviter(user.getUUID()) != null) {
                number = im.getInvitesByInviter(user.getUUID()).size();
                for (Invite invite : im.getInvitesByInviter(user.getUUID())) {
                    if (!invite.isAccepted()) {
                        number--;
                    }
                }
            }
            top.put(user.getName(), number);
        }

        Map<String, Integer> sortedMapAsc = Utils.sortByComparator(top, false);
        int limit = plugin.getConfig().getInt(Options.CONFIG_TOPLIMIT);
        for (Entry<String, Integer> entry : sortedMapAsc.entrySet()) {
            if (limit != 0) {
                builder.append("&a" + entry.getKey() + " &c>>> &b" + entry.getValue() + "\n");
                limit--;
            } else {
                break;
            }
        }

        player.sendMessage(Utils.colorizer(builder.toString()));
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

}
