package cz.apik007.referralsystem.database;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import cz.apik007.referralsystem.ReferralSystem;
import cz.apik007.referralsystem.utils.Utils;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MySQLManager {

    private ReferralSystem plugin = ReferralSystem.getInstance();

    private final String host = plugin.getDb().getString("host");
    private final String port = plugin.getDb().getString("port");
    private final String user = plugin.getDb().getString("user");
    private final String pass = plugin.getDb().getString("password");
    private final String name = plugin.getDb().getString("database");

    private MySQL db;

    public MySQLManager(ReferralSystem plugin) {
        this.plugin = plugin;
    }

    public void setup() {
        db = new MySQL(host, port, name, user, pass);

        try {
            db.openConnection();
        } catch (CommunicationsException ex) {
            Utils.sendConsoleMessage("&c[ReferralSystem] ERROR: " + ex.getMessage() + " IN: " + ex.getClass().toString());

        } catch (SQLException | ClassNotFoundException ex) {
            Utils.sendConsoleMessage("&c[ReferralSystem] ERROR: " + ex.getMessage() + " IN: " + ex.getClass().toString());

        }
        try (Statement statement = db.getConnection().createStatement()) {
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS `rsp_users` (`uuid` varchar(200) NOT NULL PRIMARY KEY, `name` varchar(200), `ip` varchar(200), `firstLog` BIGINT, `lastLog` BIGINT, `timePlayed` BIGINT)");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS `rsp_invites` (`invited` varchar(200), `inviter` varchar(200), `time` BIGINT, `accepted` TINYINT(1), `rewarded` TINYINT(1))");
        } catch (SQLException ex) {
            Utils.sendConsoleMessage("&c[ReferralSystem] ERROR " + ex.getErrorCode() + " : " + ex.getMessage() + " IN: " + ex.getClass().toString());
        }

    }

    public void closeConnection() throws SQLException {
        db.closeConnection();
    }

    public MySQL getDb() {
        return db;
    }

}
