package cz.apik007.referralsystem.inventory;

import cz.apik007.referralsystem.ReferralSystem;
import cz.apik007.referralsystem.model.Invite;
import cz.apik007.referralsystem.utils.HiddenStringUtils;
import cz.apik007.referralsystem.utils.MessagesStrings;
import cz.apik007.referralsystem.utils.Options;
import cz.apik007.referralsystem.utils.Utils;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryMenu implements Listener {

    private static Inventory homeMenu = Bukkit.createInventory(null, 54, ReferralSystem.getInstance().getMessages().getString(MessagesStrings.MENU_HOME));
    private static Inventory invDelete = Bukkit.createInventory(null, 54, ReferralSystem.getInstance().getMessages().getString(MessagesStrings.MENU_DELETE));

    private final ReferralSystem plugin = ReferralSystem.getInstance();

    public void setItem(int position, ItemStack is, Inventory inv) {
        inv.setItem(position, is);
    }

    public ItemStack createItem(Material material, String name, String lore) {
        ItemStack is = new ItemStack(material);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(Utils.colorizer(name));
        im.setLore(Arrays.asList(Utils.colorizer(lore)));
        is.setItemMeta(im);
        return is;
    }

    public void setUpHomeMenu(Player player) {
        homeMenu = Bukkit.createInventory(null, 9, Utils.colorizer(plugin.getMessages().getString(MessagesStrings.MENU_HOME)));
        invDelete = Bukkit.createInventory(null, 27, Utils.colorizer(plugin.getMessages().getString(MessagesStrings.MENU_DELETE)));

        int index = 0;

        if (player.hasPermission(Options.PERMISSION_ADMIN)) {
            int size = plugin.getInviteManager().getInvites().size();

            if (size == 0) {
                Utils.setItem(4, Utils.createItem(Material.BARRIER, plugin.getMessages().getString(MessagesStrings.MENU_INVITATIONS_NONE), plugin.getMessages().getString(MessagesStrings.MENU_INVITATIONS_NONE_LORE)), homeMenu);
                Utils.makeGlass(15, homeMenu);
                player.openInventory(homeMenu);
                return;
            }

            if (size >= 9) {
                homeMenu = Bukkit.createInventory(null, 18, Utils.colorizer(plugin.getMessages().getString(MessagesStrings.MENU_HOME)));
            }
            if (size >= 18) {
                homeMenu = Bukkit.createInventory(null, 27, Utils.colorizer(plugin.getMessages().getString(MessagesStrings.MENU_HOME)));
            }
            if (size >= 27) {
                homeMenu = Bukkit.createInventory(null, 36, Utils.colorizer(plugin.getMessages().getString(MessagesStrings.MENU_HOME)));
            }
            if (size >= 36) {
                homeMenu = Bukkit.createInventory(null, 45, Utils.colorizer(plugin.getMessages().getString(MessagesStrings.MENU_HOME)));
            }
            if (size >= 45) {
                homeMenu = Bukkit.createInventory(null, 54, Utils.colorizer(plugin.getMessages().getString(MessagesStrings.MENU_HOME)));
            }
            for (Invite invite : plugin.getInviteManager().getInvites()) {
                ItemStack is = createItem(Material.TRIPWIRE_HOOK, "&c&l" + invite.getInvited(), plugin.getMessages().getString(MessagesStrings.MENU_INV_STATE) + String.valueOf(invite.isAccepted()));
                ItemMeta meta = is.getItemMeta();
                List<String> lore = meta.getLore();
                lore.add(Utils.colorizer("&c&l" + invite.getInvited() + plugin.getMessages().getString(MessagesStrings.MENU_ADMIN_INVITEDBY) + plugin.getUserManager().getUserByUUID(UUID.fromString(invite.getInviter())).getName()));
                lore.add(HiddenStringUtils.encodeString(invite.getInvited()));
                meta.setLore(lore);
                is.setItemMeta(meta);
                setItem(index, is, homeMenu);
                index++;

            }
        } else {
            int size = plugin.getInviteManager().getInvitesByInviter(player.getName()).size();

            if (size == 0) {
                Utils.setItem(4, Utils.createItem(Material.BARRIER, plugin.getMessages().getString(MessagesStrings.MENU_INVITATIONS_NONE), plugin.getMessages().getString(MessagesStrings.MENU_INVITATIONS_NONE_LORE)), homeMenu);
                Utils.makeGlass(15, homeMenu);
                player.openInventory(homeMenu);
                return;
            }
            if (size >= 9) {
                homeMenu = Bukkit.createInventory(null, 18, Utils.colorizer(plugin.getMessages().getString(MessagesStrings.MENU_HOME)));
            }
            if (size >= 18) {
                homeMenu = Bukkit.createInventory(null, 27, Utils.colorizer(plugin.getMessages().getString(MessagesStrings.MENU_HOME)));
            }
            if (size >= 27) {
                homeMenu = Bukkit.createInventory(null, 36, Utils.colorizer(plugin.getMessages().getString(MessagesStrings.MENU_HOME)));
            }

            for (Invite invite : plugin.getInviteManager().getInvitesByInviter(player.getName())) {
                ItemStack is = createItem(Material.TRIPWIRE_HOOK, "&c&l" + invite.getInvited(), plugin.getMessages().getString(MessagesStrings.MENU_INV_STATE) + String.valueOf(invite.isAccepted()));

                ItemMeta meta = is.getItemMeta();
                List<String> lore = meta.getLore();
                lore.add(HiddenStringUtils.encodeString(invite.getInvited()));
                meta.setLore(lore);
                is.setItemMeta(meta);
                setItem(index, is, homeMenu);
                index++;
            }
        }

        Utils.makeGlass(15, homeMenu);

        player.openInventory(homeMenu);
    }

    public void setUpDeleteMenu(String invitedName, Player player) {

        Invite invitation = plugin.getInviteManager().getInviteByInvited(invitedName);

        ItemStack request = createItem(Material.BOOK, "&c&lACTION&b", plugin.getMessages().getString(MessagesStrings.MENU_INV_CONFIRM));
        ItemMeta im = request.getItemMeta();
        List<String> lore = im.getLore();
        lore.add(Utils.colorizer(plugin.getMessages().getString(MessagesStrings.REFERRER) + plugin.getUserManager().getUserByUUID(UUID.fromString(invitation.getInviter())).getName()));
        lore.add(Utils.colorizer(plugin.getMessages().getString(MessagesStrings.INVITED_PLAYER) + invitation.getInvited()));
        lore.add(HiddenStringUtils.encodeString(invitedName));
        im.setLore(lore);
        request.setItemMeta(im);
        setItem(9, request, invDelete);

        if (!invitation.isAccepted()) {
            ItemStack accept = createItem(Material.EMERALD_BLOCK, plugin.getMessages().getString(MessagesStrings.MENU_INV_DELETE), "");
            setItem(14, accept, invDelete);
            ItemStack deny = createItem(Material.REDSTONE_BLOCK, plugin.getMessages().getString(MessagesStrings.MENU_INV_LETBE), "");
            setItem(15, deny, invDelete);
        } else {
            ItemStack info = createItem(Material.TRIPWIRE_HOOK, plugin.getMessages().getString(MessagesStrings.MENU_INV_ALREADY_DONE), "");
            setItem(15, info, invDelete);
        }

        ItemStack back = createItem(Material.ARROW, plugin.getMessages().getString(MessagesStrings.MENU_BACK), "");
        setItem(26, back, invDelete);

        Utils.makeGlass(15, invDelete);

        player.openInventory(invDelete);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) throws SQLException, ClassNotFoundException {

        try {
            Player player = (Player) event.getWhoClicked();
            ItemStack clicked = event.getCurrentItem();
            Inventory inventory = event.getInventory();

            if (clicked != null) {
                if (inventory.getName().equals(homeMenu.getName())) {
                    event.setCancelled(true);
                    player.closeInventory();
                    if (clicked.getType() == Material.TRIPWIRE_HOOK) {
                        setUpDeleteMenu(HiddenStringUtils.extractHiddenString(clicked.getItemMeta().getLore().get(clicked.getItemMeta().getLore().size() - 1)), player);
                    }

                } else if (inventory.getName().equals(invDelete.getName())) {
                    event.setCancelled(true);
                    player.closeInventory();

                    ItemStack item = inventory.getItem(9);
                    ItemMeta im = item.getItemMeta();
                    String invited = HiddenStringUtils.extractHiddenString(im.getLore().get(3));
                    switch (clicked.getType()) {
                        case EMERALD_BLOCK:
                            plugin.getController().removeInvitation(player, invited);
                            break;
                        case REDSTONE_BLOCK:
                            setUpHomeMenu(player);
                            break;
                        case ARROW:
                            setUpHomeMenu(player);

                    }
                }
            }
        } catch (NullPointerException ex) {
            System.out.println("[ReferralSystemPro] WARNING! Internal error happened in InventoryMenu (Note: This error will not do anything with the game)");
        }
    }
}
