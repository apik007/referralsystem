/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.apik007.referralsystem;

import cz.apik007.referralsystem.utils.Options;
import cz.apik007.referralsystem.utils.Utils;
import cz.apik007.referralsystem.controller.Controller;
import cz.apik007.referralsystem.database.MySQLManager;
import cz.apik007.referralsystem.inventory.InventoryMenu;
import cz.apik007.referralsystem.listeners.ActivityListener;
import cz.apik007.referralsystem.model.InviteManager;
import cz.apik007.referralsystem.model.SecurityUserManager;
import cz.apik007.referralsystem.model.UserManager;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Apik
 */
public class ReferralSystem extends JavaPlugin {

    private static ReferralSystem instance;

    private File playersFile;
    private FileConfiguration players;

    private File invitesFile;
    private FileConfiguration invites;

    private File messagesFile;
    private FileConfiguration messages;

    private File configFile;
    private FileConfiguration config;

    private File dbFile;
    private FileConfiguration db;

    private UserManager userManager;
    private InviteManager inviteManager;
    private SecurityUserManager securityUserManager;
    private Controller controller;

    private MySQLManager mysqlManager;
    private boolean mysqlSetup = true;

    private static final Logger log = Logger.getLogger("Minecraft");

    @Override
    public void onEnable() {
        instance = this;
        loadConfigs();

        if (mysqlSetup) {
            mysqlManager = new MySQLManager(this);
            mysqlManager.setup();
        }
        try {
            userManager = new UserManager();
            inviteManager = new InviteManager();
        } catch (Exception ex) {

            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement se = ex.getStackTrace()[i];
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }

        securityUserManager = new SecurityUserManager();
        controller = new Controller();
        registerCommands();
        registerListeners();
        try {
            controller.maybeReload();
        } catch (Exception ex) {

            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement se = ex.getStackTrace()[i];
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }
        Utils.sendConsoleMessage("&a[ReferralSystem] INFO: Plugin successfully enabled!");

        int sec = 15;
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                for (Player player : getServer().getOnlinePlayers()) {
                    try {
                        securityUserManager.timeCounter(player);
                        controller.checkReward(player);
                    } catch (Exception ex) {

                        Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
                        for (int i = 0; i < ex.getStackTrace().length; i++) {
                            StackTraceElement se = ex.getStackTrace()[i];
                            Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
                        }

                    }
                }

            }
        }, 20L * sec, 20L * sec);

    }

    @Override
    public void onDisable() {
        Utils.sendConsoleMessage("&c[ReferralSystem] INFO: Plugin successfully disabled!");
    }

    public void loadConfigs() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        messagesFile = new File(getDataFolder(), "messages.yml");
        if (!messagesFile.exists()) {
            try {
                System.out.println("[ReferralSystemPro] messages.yml not found, was it deleted? Creating new messages file...");
                messagesFile.createNewFile();
            } catch (Exception ex) {

                Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
                for (int i = 0; i < ex.getStackTrace().length; i++) {
                    StackTraceElement se = ex.getStackTrace()[i];
                    Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
                }

            }
        }
        messages = YamlConfiguration.loadConfiguration(messagesFile);
        Utils.setUpMessages(this);

        configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                System.out.println("[ReferralSystem] WARNING: config.yml not found, was it deleted? Creating new config file...");
                configFile.createNewFile();
            } catch (Exception ex) {

                Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
                for (int i = 0; i < ex.getStackTrace().length; i++) {
                    StackTraceElement se = ex.getStackTrace()[i];
                    Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
                }

            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);

        if (!config.contains(Options.CONFIG_TOPLIMIT)) {
            config.set(Options.CONFIG_TOPLIMIT, 10);
        }
        if (!config.contains(Options.CONFIG_TIMELIMIT)) {
            config.set(Options.CONFIG_TIMELIMIT, 7200);
        }
        if (!config.contains(Options.CONFIG_MAX_ACCEPTABLE_PLAYED_TIME)) {
            config.set(Options.CONFIG_MAX_ACCEPTABLE_PLAYED_TIME, 0);
        }
        if (!config.contains(Options.AFK_TIME)) {
            config.set(Options.AFK_TIME, 25);
        }
        if (!config.contains(Options.CONFIG_SECTION_COMMANDS)) {
            config.set(Options.CONFIG_SECTION_COMMANDS, "");
        }
        if (!config.contains(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_INVITED)) {
            config.set(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_INVITED, "");
        }
        if (config.getStringList(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_INVITED + "." + Options.CONFIG_SECTION_COMMANDS_NORMAL).isEmpty()) {
            List<String> blocksBlocked = config.getStringList(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_INVITED + "." + Options.CONFIG_SECTION_COMMANDS_NORMAL);
            String number = "eco give %invited% 2500";
            blocksBlocked.add(number);
            config.set(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_INVITED + "." + Options.CONFIG_SECTION_COMMANDS_NORMAL, blocksBlocked);

            List<String> blocksBlocked02 = config.getStringList(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_INVITED + "." + 5);
            String number02 = "eco give %invited% 5000";
            blocksBlocked02.add(number02);
            config.set(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_INVITED + "." + 5, blocksBlocked02);

        }
        if (!config.contains(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_REFERRER)) {
            config.set(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_REFERRER, "");
        }
        if (config.getStringList(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_REFERRER + "." + Options.CONFIG_SECTION_COMMANDS_NORMAL).isEmpty()) {
            List<String> blocksBlocked = config.getStringList(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_REFERRER + "." + Options.CONFIG_SECTION_COMMANDS_NORMAL);
            String number = "eco give %referrer% 5000";
            blocksBlocked.add(number);
            config.set(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_REFERRER + "." + Options.CONFIG_SECTION_COMMANDS_NORMAL, blocksBlocked);

            List<String> blocksBlocked02 = config.getStringList(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_REFERRER + "." + 5);
            String number02 = "eco give %referrer% 100000";
            blocksBlocked02.add(number02);
            config.set(Options.CONFIG_SECTION_COMMANDS + "." + Options.CONFIG_SECTION_COMMANDS_REFERRER + "." + 5, blocksBlocked02);

        }

        try {
            config.save(configFile);
            System.out.println("[ReferralSystem] INFO: config.yml loaded");
        } catch (Exception ex) {

            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement se = ex.getStackTrace()[i];
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }

        dbFile = new File(getDataFolder(), "dbconfig.yml");
        if (!dbFile.exists()) {
            try {
                System.out.println("[ReferralSystem] WARNING: dbconfig.yml not found, was it deleted? Creating new database config file...");
                dbFile.createNewFile();
            } catch (Exception ex) {

                Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
                for (int i = 0; i < ex.getStackTrace().length; i++) {
                    StackTraceElement se = ex.getStackTrace()[i];
                    Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
                }

            }
        }

        db = YamlConfiguration.loadConfiguration(dbFile);

        if (!db.contains("host")) {
            db.set("host", "localhost");
            Utils.sendConsoleMessage("&c[ReferralSystem] ERROR: Database is not set up - please, edit your dbconfig.yml");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        if (!db.contains("port")) {
            db.set("port", "3306");
            Utils.sendConsoleMessage("&c[ReferralSystem] ERROR: Database is not set up - please, edit your dbconfig.yml");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        if (!db.contains("database")) {
            db.set("database", "db");
            Utils.sendConsoleMessage("&c[ReferralSystem] ERROR: Database is not set up - please, edit your dbconfig.yml");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        if (!db.contains("user")) {
            db.set("user", "root");
            Utils.sendConsoleMessage("&c[ReferralSystem] ERROR: Database is not set up - please, edit your dbconfig.yml");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        if (!db.contains("password")) {
            db.set("password", "pass12345");
            Utils.sendConsoleMessage("&c[ReferralSystem] ERROR: Database is not set up - please, edit your dbconfig.yml");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        try {
            db.save(dbFile);
            System.out.println("[ReferralSystem] INFO: dbconfig.yml loaded");
        } catch (Exception ex) {

            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement se = ex.getStackTrace()[i];
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }

    }

    public void registerCommands() {
        try {
            getCommand(Options.CMD_NAME).setExecutor(new Commands());
            getCommand(Options.CMD_ALIAS).setExecutor(new Commands());
        } catch (Exception ex) {
            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (StackTraceElement se : ex.getStackTrace()) {
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }
    }

    public void registerListeners() {
        try {
            this.getServer().getPluginManager().registerEvents(new ActivityListener(), this);
            this.getServer().getPluginManager().registerEvents(new InventoryMenu(), this);
        } catch (Exception ex) {
            Utils.sendConsoleMessage("&c[ReferralSystem] INTERNAL ERROR: " + ex.getMessage() + " EXCEPTION: " + ex.getClass().toString());
            for (StackTraceElement se : ex.getStackTrace()) {
                Utils.sendConsoleMessage("&e[ReferralSystem] FILE: &b" + se.getFileName() + " &eLINE: &b" + se.getLineNumber() + "&e METHOD: &b" + se.getMethodName());
            }

        }
    }

    public static ReferralSystem getInstance() {
        return instance;
    }

    public File getPlayersFile() {
        return playersFile;
    }

    public FileConfiguration getPlayers() {
        return players;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public File getInvitesFile() {
        return invitesFile;
    }

    public FileConfiguration getInvites() {
        return invites;
    }

    public InviteManager getInviteManager() {
        return inviteManager;
    }

    public Controller getController() {
        return controller;
    }

    public File getMessagesFile() {
        return messagesFile;
    }

    public FileConfiguration getMessages() {
        return messages;
    }

    public SecurityUserManager getSecurityUserManager() {
        return securityUserManager;
    }

    public File getDbFile() {
        return dbFile;
    }

    public FileConfiguration getDb() {
        return db;
    }

    public MySQLManager getMysqlManager() {
        return mysqlManager;
    }

    public boolean isMysqlSetup() {
        return mysqlSetup;
    }

}
