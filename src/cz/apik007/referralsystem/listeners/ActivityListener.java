package cz.apik007.referralsystem.listeners;

import cz.apik007.referralsystem.utils.Options;
import cz.apik007.referralsystem.ReferralSystem;
import cz.apik007.referralsystem.model.Invite;
import cz.apik007.referralsystem.model.SecurityUser;
import cz.apik007.referralsystem.model.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ActivityListener implements Listener {

    private ReferralSystem plugin = ReferralSystem.getInstance();

    @EventHandler
    public void onJoin(PlayerJoinEvent event) throws SQLException, ClassNotFoundException {
        Player player = event.getPlayer();

        plugin.getController().join(player);

    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) throws SQLException, ClassNotFoundException {
        plugin.getSecurityUserManager().checkUser(event.getPlayer());
        plugin.getSecurityUserManager().timeCounter(event.getPlayer());
        plugin.getSecurityUserManager().removeUser(event.getPlayer().getName());

    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        //Vezme frajera z eventu
        Player player = event.getPlayer();
        //Uloží aktuální čas
        plugin.getSecurityUserManager().checkUser(player);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        //Vezme frajera z eventu
        Player player = event.getPlayer();
        //Uloží aktuální čas
        plugin.getSecurityUserManager().checkUser(player);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        //Vezme frajera z eventu
        Player player = event.getPlayer();
        //Uloží aktuální čas
        plugin.getSecurityUserManager().checkUser(player);
    }

}
