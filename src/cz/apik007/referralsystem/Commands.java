/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.apik007.referralsystem;

import cz.apik007.referralsystem.inventory.InventoryMenu;
import cz.apik007.referralsystem.utils.MessagesStrings;
import cz.apik007.referralsystem.utils.Options;
import cz.apik007.referralsystem.utils.Utils;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Apik007
 */
public class Commands implements CommandExecutor {

    private ReferralSystem plugin = ReferralSystem.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {

        if (!cmd.getName().equalsIgnoreCase(Options.CMD_NAME) && !cmd.getName().equalsIgnoreCase(Options.CMD_ALIAS)) {
            return false;
        }
        if (!(sender instanceof Player)) {
            sender.sendMessage(plugin.getMessages().getString(MessagesStrings.INGAME_ONLY));
            return true;
        }

        Player player = (Player) sender;
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase(Options.ARG_LIST)) {
                plugin.getController().list(player);
                return true;
            }
            if (args[0].equalsIgnoreCase(Options.ARG_LIMIT)) {
                player.sendMessage(Utils.colorizer(plugin.getMessages().getString(MessagesStrings.PREXIF) + plugin.getMessages().getString(MessagesStrings.ACTUAL_LIMIT) + Utils.getLimit(player, Options.PERMISSION_LIMIT)));
                String msg = plugin.getMessages().getString(MessagesStrings.INVITATIONS_ALREADY_SENT);
                msg = msg.replace("%invites%", Integer.toString(plugin.getInviteManager().getInvitesByInviter(player.getName()).size()));
                player.sendMessage(Utils.colorizer(msg));
                /*if (player.hasPermission(Options.PERMISSION_VIP)) {
                 player.sendMessage(Utils.colorizer(plugin.getMessages().getString(MessagesStrings.ACTUAL_LIMIT) + plugin.getConfig().getInt(Options.CONFIG_LIMIT)));
                 } else {
                 player.sendMessage(Utils.colorizer(plugin.getMessages().getString(MessagesStrings.ACTUAL_LIMIT) + plugin.getConfig().getInt(Options.CONFIG_VIP_LIMIT)));
                 }*/
                return true;
            }
            if (args[0].equalsIgnoreCase(Options.ARG_HELP)) {
                plugin.getController().helpMenu(player);
                return true;
            }
            if (args[0].equalsIgnoreCase(Options.ARG_MENU)) {
                InventoryMenu menu = new InventoryMenu();
                menu.setUpHomeMenu(player);
                return true;
            }
            if (args[0].equalsIgnoreCase(Options.ARG_TOP)) {
                plugin.getController().topMenu(player);
                return true;
            }
            plugin.getController().helpMenu(player);
            return true;
        }
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase(Options.ARG_INVITE)) {

                plugin.getController().invitePlayer(player, args[1]);

            }
            if (args[0].equalsIgnoreCase(Options.ARG_INFO)) {
                plugin.getController().info(player, args[1]);
                return true;
            }
            if (args[0].equalsIgnoreCase(Options.ARG_BYPASS)) {
                if (!player.hasPermission(Options.PERMISSION_ADMIN)) {
                    Utils.sendRegularMessage(player, MessagesStrings.NO_PERMISSION);
                    return true;
                }

                String invited = args[1];
                try {
                    plugin.getInviteManager().acceptInvite(invited);
                } catch (SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(Commands.class.getName()).log(Level.SEVERE, null, ex);
                }
                Utils.sendRegularMessage(player, MessagesStrings.SUCCESSFULLY_BYPASSED);
                return true;

            }
            return true;
        }
        plugin.getController().helpMenu(player);
        return true;

    }

}
